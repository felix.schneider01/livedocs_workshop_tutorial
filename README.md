# LiveDocs Workshop Tutorial

## Badges

### Public CRC Binderhub Links
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://c109-005.cloud.gwdg.de:30901/v2/gwdg/pedrocklein%2Flivedocs_workshop_tutorial/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://c109-005.cloud.gwdg.de:30901/v2/gwdg/pedrocklein%2Flivedocs_workshop_tutorial/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://c109-005.cloud.gwdg.de:30901/v2/gwdg/pedrocklein%2Flivedocs_workshop_tutorial/HEAD?urlpath=voila)

### JupyterLite Link
[![lite-badge](https://img.shields.io/badge/CRC1456-Jupyterlite-yellow)](https://felix.schneider01.pages.gwdg.de/livedocs_workshop_tutorial)

### Static HTMLs
[![static-html](https://img.shields.io/badge/CRC1456-Hello_World-white)](https://felix.schneider01.pages.gwdg.de/livedocs_workshop_tutorial/files/hello_world.html)
[![static-html](https://img.shields.io/badge/CRC1456-data-science-best-practices-using-pandas-titanic-white)](https://felix.schneider01.pages.gwdg.de/livedocs_workshop_tutorial/files/data-science-best-practices-using-pandas-titanic.html)
[![static-html](https://img.shields.io/badge/CRC1456-plot_iris_dataset-white)](https://felix.schneider01.pages.gwdg.de/livedocs_workshop_tutorial/files/plot_iris_dataset.html)

### Docker Image at GWDG Gitlab Docker Registry
[![Docker](https://img.shields.io/badge/CRC1456-Dockerhub-blue)](https://gitlab.gwdg.de/felix.schneider01/livedocs_workshop_tutorial/container_registry/3031)

